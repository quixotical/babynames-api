var express = require('express');
var router = express.Router();
const cassandra = require('cassandra-driver');
const client = new cassandra.Client({contactPoints: ['127.0.0.1'], keyspace: 'babynames'});
const assert = require('assert');

client.connect(function(err){
  assert.ifError(err)
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/babynames', function(req, res, next){
  const query = 'SELECT name from names LIMIT 5';

  client.execute(query, function(err, result){
    assert.ifError(err);
    res.send(result.rows);
  });
});

module.exports = router;
